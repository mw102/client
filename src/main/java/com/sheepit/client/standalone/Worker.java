package com.sheepit.client.standalone;

/**
 * For backward compatibility with the auto updater launcher
 */
public class Worker {
	public static void main(String[] args) {
		com.sheepit.client.main.Worker.main(args);
	}
}
