/*
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.sheepit.client.rendering;

import lombok.Data;

/**
 * Track if the render ask been cancel, is finished...
 */
@Data public class RenderState {
	private boolean askForRendererKill = false;
	private boolean userBlock = false;
	private boolean serverBlock = false;
	private boolean incompatibleProcessKill = false;
	
	public void setBlockIncompatibleProcess() {
		setAskForRendererKill(true);
		setIncompatibleProcessKill(true);
	}
	
	public void setBlock() {
		setAskForRendererKill(true);
		setUserBlock(true);
	}
}
